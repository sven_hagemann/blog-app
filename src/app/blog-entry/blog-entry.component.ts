import { Component, OnInit, Input } from '@angular/core';
import { BlogEntry } from '../blog-entry';

@Component({
  selector: 'ch-blog-entry',
  templateUrl: './blog-entry.component.html',
  styleUrls: ['./blog-entry.component.css']
})
export class BlogEntryComponent implements OnInit {

  @Input() entry: BlogEntry;

  constructor() { }

  ngOnInit() {
  }

}
