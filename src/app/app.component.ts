import { Component } from '@angular/core';
import { BlogEntry } from './blog-entry';

@Component({
  selector: 'ch-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  entries: Array<BlogEntry> = [];

  createBlogEntry(title: string, image: string, text: string) {
    const entry = new BlogEntry();
    entry.image = image;
    entry.text = text;
    entry.title = title;

    this.entries.push(entry);
  }
}

